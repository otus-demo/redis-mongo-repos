﻿using Redis.OM.Modeling;

namespace MongoRedisAsDataStorageInWebApplicationExample.Core.Domain
{
    [Document(StorageType = StorageType.Json)]
    public class WeatherForecast
    {
        [RedisIdField]
        public string Id{ get; set; } = Guid.NewGuid().ToString(); //Ulid
        [Indexed]
        public string CityName { get; set; }
        [Indexed]
        public GeoLoc GeoLoc { get; set; }
        [Indexed]
        public DateOnly Date { get; set; }
        [Indexed]
        public int TemperatureC { get; set; }
    }
}
